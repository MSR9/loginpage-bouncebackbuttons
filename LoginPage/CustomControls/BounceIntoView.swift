//
//  BounceIntoView.swift
//  LoginScreenExample
//
//  Created by Prahlad Reddy on 12/21/16.
//  Copyright © 2016 Prahlad Reddy. All rights reserved.
//

import UIKit

class BounceIntoView: UIButton {

    override func awakeFromNib() {
        transform = CGAffineTransform(scaleX: 0.3, y: 0.3)
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 6, options: .allowUserInteraction, animations: {
            self.transform = CGAffineTransform.identity
        }, completion: nil)
    }
}
