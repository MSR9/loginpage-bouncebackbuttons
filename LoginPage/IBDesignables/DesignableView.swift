//
//  DesignableView.swift
//  LoginScreenExample
//
//  Created by Prahlad Reddy on 11/29/16.
//  Copyright © 2016 Prahlad Reddy. All rights reserved.
//

import UIKit

@IBDesignable class DesignableView: UIView {
   
    override func draw(_ rect: CGRect) {
        self.clipsToBounds = true
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
}
