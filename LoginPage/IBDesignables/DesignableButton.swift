//
//  DesignableButton.swift
//  LoginScreenExample
//
//  Created by Prahlad Reddy on 11/26/16.
//  Copyright © 2016 Prahlad Reddy. All rights reserved.
//

import UIKit

@IBDesignable class DesignableButton: BounceButton {
    
    override func draw(_ rect: CGRect) {
        self.clipsToBounds = true
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            self.layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0.0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }

    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            self.layer.borderColor = borderColor.cgColor
        }
    }
}
