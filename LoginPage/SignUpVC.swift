//
//  SignUpVC.swift
//  LoginScreenExample
//
//  Created by Mark Moeykens on 11/29/16.
//  Copyright © 2016 Mark Moeykens. All rights reserved.
//

import UIKit

class SignUpVC: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var scrollView: UIScrollView!
    var keyboardIsShowing = false
    var activeField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default
            .addObserver(self, selector: #selector(keyboardDidShow(notification:)),
                         name: UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default
            .addObserver(self, selector: #selector(keyboardWillHide),
                         name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func keyboardDidShow(notification: NSNotification) {
        
        if keyboardIsShowing { return }
        
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey]
            as? NSValue)?.cgRectValue {
            keyboardIsShowing = true
            
            scrollView.contentInset = UIEdgeInsets(top: 0,
                                                   left: 0,
                                                   bottom: keyboardSize.height,
                                                   right: 0)
            scrollView.scrollIndicatorInsets = self.scrollView.contentInset
            
            var visibleArea = self.view.frame
            visibleArea.size.height -= keyboardSize.height
            
            if (!visibleArea.contains(activeField.frame.origin) ) {
                let frame = activeField.frame
                let rect = CGRect(x: frame.origin.x, y: frame.origin.y + 50, width: frame.width, height: frame.height)
                scrollView.scrollRectToVisible(rect, animated: true)
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey]
            as? NSValue)?.cgRectValue {
            keyboardIsShowing = false
            view.frame.origin.y += keyboardSize.height
        }
    }
    
    // Set all your textfields' Editing Did Begin event to this.
    @IBAction func editingDidBegin(_ sender: UITextField) {
        activeField = sender
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    @IBAction func signUp(_ sender: UIButton) {
        // Sign up logic here
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
